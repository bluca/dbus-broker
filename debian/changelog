dbus-broker (36-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/36'
  * Bump Standards-Version to 4.7.0, no changes
  * apparmor patch: refresh and use DEP-3 header

 -- Luca Boccassi <bluca@debian.org>  Sat, 13 Apr 2024 12:16:38 +0100

dbus-broker (35-2) unstable; urgency=medium

  * Build-depend on systemd-dev instead of systemd
  * d/p/github_apparmor_support.patch: enable apparmor support
  * Build depend on pkgconf instead of pkg-config
  * Drop override for deprecated tag package-supports-alternative-init-
    but-no-init.d-script

 -- Luca Boccassi <bluca@debian.org>  Fri, 16 Feb 2024 11:42:07 +0000

dbus-broker (35-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/35'
    For more details see the upstream changelog:
    https://github.com/bus1/dbus-broker/releases/tag/v35

 -- Luca Boccassi <bluca@debian.org>  Sun, 24 Dec 2023 01:31:54 +0100

dbus-broker (34-1) unstable; urgency=medium

  * Enable ELF metadata stamping
  * Update upstream source from tag 'upstream/34'
    For more details see the upstream changelog:
    https://github.com/bus1/dbus-broker/releases/tag/v34
    (Closes: #1043177)
  * Refresh Lintian overrides

 -- Luca Boccassi <bluca@debian.org>  Thu, 14 Dec 2023 11:00:17 +0000

dbus-broker (33-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/33'
    For more details see the upstream changelog:
    https://github.com/bus1/dbus-broker/releases/tag/v33
  * postinst: remove hard dependency on sysv-utils

 -- Luca Boccassi <bluca@debian.org>  Mon, 06 Feb 2023 17:11:47 +0000

dbus-broker (32-2) unstable; urgency=medium

  * Add upstream signing key
  * d/watch: fetch signature, use api.github.com
  * Bump Standards-Version to 4.6.2, no changes
  * Bump d/copyright year range

 -- Luca Boccassi <bluca@debian.org>  Sun, 15 Jan 2023 16:30:34 +0000

dbus-broker (32-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/32'
  * Enable apparmor support
  * Update Lintian overrides

 -- Luca Boccassi <bluca@debian.org>  Sat, 06 Aug 2022 13:37:31 +0100

dbus-broker (31-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/31'
  * Drop patches merged upstream

 -- Luca Boccassi <bluca@debian.org>  Tue, 17 May 2022 22:33:29 +0100

dbus-broker (30-2) unstable; urgency=medium

  * Backport fixes for assert and memory leaks from upstream (Closes:
    #1010898)
  * Bump Standards-Version to 4.6.1, no changes

 -- Luca Boccassi <bluca@debian.org>  Thu, 12 May 2022 21:30:25 +0100

dbus-broker (30-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/30'
  * Drop patches merged upstream
  * Update d/copyright to fix Lintian warnings

 -- Luca Boccassi <bluca@debian.org>  Tue, 10 May 2022 19:48:03 +0100

dbus-broker (29-4) unstable; urgency=medium

  * Backport patches to fix activation of units with failing Condition*=
    checks
  * Bump d/copyright year range
  * Update Lintian overrides syntax
  * Note that 0002-Run-user-service-in-session.slice.patch was merged
    upstream

 -- Luca Boccassi <bluca@debian.org>  Sun, 16 Jan 2022 19:52:37 +0000

dbus-broker (29-3) unstable; urgency=medium

  * Backport patch to run user broker in session.slice

 -- Luca Boccassi <bluca@debian.org>  Sun, 31 Oct 2021 23:41:59 +0000

dbus-broker (29-2) unstable; urgency=medium

  * Refresh 0001-Restore-compatibility-with-older-libaudit-
    libselinux.patch
  * Depend on dbus-system-bus-common and provide dbus-system-bus
  * Bump Standards-Version to 4.6.0, no changes
  * Update lintian-override syntax
  * Add upstream/metadata file

 -- Luca Boccassi <bluca@debian.org>  Fri, 29 Oct 2021 00:09:26 +0100

dbus-broker (29-1) unstable; urgency=medium

  * Move package maintenance to the Utopia Team
  * Update upstream source from tag 'upstream/29'
  * Drop deprecated 'linux-4-17' build option
  * Backport patch to restore compatibility with libselinux 3.0

 -- Luca Boccassi <bluca@debian.org>  Sun, 15 Aug 2021 13:37:08 +0100

dbus-broker (26-1) unstable; urgency=low

  * Update upstream source from tag 'upstream/26'
  * maintscript: do reboot notification on first install too (Closes:
    #980540)
  * Do not stop the service on package removal (Closes: #980541)
  * Do not start after installation
  * Do not fail the installation if a reload fails
  * Add autopkgtest and run unit tests

 -- Luca Boccassi <bluca@debian.org>  Fri, 22 Jan 2021 00:00:39 +0000

dbus-broker (25-2) unstable; urgency=low

  * Upload to unstable, no changes.

 -- Luca Boccassi <bluca@debian.org>  Sun, 17 Jan 2021 14:40:56 +0000

dbus-broker (25-1) experimental; urgency=medium

  [ Daniele Nicolodi ]
  * Initial release. Closes: #892001
  * Install NEWS file as package upstream changelog.
  * Bump Standards-Version to 4.1.4. No changes required.
  * Bump debhelper compat to 11. No changes required.
  * Do not restart the system bus daemon on upgrade.
  * New upstream release 12.
  * New upstream release 13.
  * Extend README.Debian.
  * Suggest a reboot on upgrade or when replacig dbus-deamon.
  * Check $DEB_BUILD_OPTIONS in override_dh_auto_test.
  * Enable all hardening features.
  * New upstream release 14.

  [ Luca Boccassi ]
  * gbp.conf: change main branch to debian/sid
  * Update d/watch file to get release tarball instead of git archive
  * Update upstream source from tag 'upstream/25'
  * Ship NEWS.md (converted from NEWS)
  * Build-Depend on libcap-ng-dev for audit
  * Convert to python3-docutils
  * Remove dependency on libglib, no longer necessary
  * Do not build documentation with nodoc profile/options
  * Remove dbus-broker-docs.docs, no such package
  * Remove dh_auto_test override, no longer needed
  * Update Vcs links in d/control
  * Add debian/* section to d/copyright
  * Bump Standards-Version to 4.5.1
  * Update d/copyright
  * Switch to debhelper-compat 13
  * Enable build-time modern kernel features
  * Update /run/reboot-required.pkgs on upgrade
  * Add Provides: dbus-system-bus, dbus-session-bus
  * Add myself as Maintainer
  * Mark dbus-broker as Multi-Arch: foreign
  * Remove user/dbus.service symlink, no longer necessary
  * Reload on upgrade
  * Add dpkg triggers for dbus config files
  * Add lintian override file
  * Remove Provides: dbus-session-bus, dependency reversed

 -- Luca Boccassi <bluca@debian.org>  Sun, 20 Dec 2020 23:18:55 +0000
